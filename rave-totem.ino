#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>

#define BAUD_RATE 9600 // Talk to BT-06 at this baud rate

#define LEDPIN1 3
#define LEDPIN2 5
#define LEDREFRESHRATE 0 // Rate at which the screen will update
#define LEDMATRIXWIDTH  64
#define LEDMATRIXHEIGHT 8

#define FONTWIDTH 6

#define MAXMSGLENGTH  32

#define RED 0
#define GREEN 1
#define BLUE  2
#define BRIGHTNESS 16

// Bluetooth Setup
SoftwareSerial bluetooth(9, 10); // RX, TX
char charBuf[MAXMSGLENGTH + 2]; // A buffer to hold the receieved message

// LED Matrix setup
byte pin = LEDPIN1; // Set initial pin to 3.
Adafruit_NeoMatrix* matrix = new Adafruit_NeoMatrix(LEDMATRIXWIDTH, LEDMATRIXHEIGHT, pin,
    NEO_MATRIX_BOTTOM    + NEO_MATRIX_RIGHT +
    NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
    NEO_GRB            + NEO_KHZ800);
short x = matrix->width();
byte color = RED;
short maxDisplacement; // 

void setup() {
  
  bluetooth.begin(BAUD_RATE);
  matrix->setTextWrap(false);
  matrix->setBrightness(BRIGHTNESS); // Moderately bright
  matrix->setTextColor(matrix->Color(255, 0, 0)); // Red
  matrix->setRotation(2); // Rotate the text 180 degrees
  
}

void loop() {
  matrix->begin(); // Initialize / Reinitialize, keeping in mind the data PIN changes every loop
  matrix->fillScreen(0);
  matrix->setCursor(x, 0);

  if (bluetooth.available()) { // If there's a new message being sent
    memset(charBuf, 0, sizeof(charBuf)); // Reset the variable that stores the message
    byte i = 0;
    while (bluetooth.available()) { // While there is more to be read, keep reading.
      charBuf[i] += (char)bluetooth.read();
      i++;
    }

    // Send back what was received, to be validated on the other side
    bluetooth.write(strcat(charBuf, "\n"));

    // Update maxDisplacement for improved scrolling (Doesn't reset scroll early / too late)
    maxDisplacement = strlen(charBuf) * FONTWIDTH + matrix->width();
  }

  matrix->print(charBuf);

  // Every loop, swap between digital output pins 3 and 5
  if (pin == LEDPIN1) {
    pin = LEDPIN2;
    if (--x < -maxDisplacement) { // Every other loop, move the cursor. If this is false, it's time to reset the scroll back to the beginning
      x = matrix->width(); // Reset the scrolling if it gets too far off screen

      // Change the color
      if (color == BLUE) {
        color = RED;
      } else {
        color++;
      }
      switch (color) {
        case RED:
          matrix->setTextColor(matrix->Color(255, 0, 0)); // Red
          break;
        case GREEN:
          matrix->setTextColor(matrix->Color(0, 255, 0)); // Green
          break;
        case BLUE:
          matrix->setTextColor(matrix->Color(0, 0, 255)); // Blue
      }
    }
  } else {
    pin = LEDPIN1;
  }

  matrix->setPin(pin); // Perform the actual pin update

  matrix->show();
  delay(LEDREFRESHRATE); // Rate at which the screen will update. Since it's mirrored, it's effectively double this
}
